package kz.aitu.oop.examples.patterns.singletone;

public class Singleton {

    private volatile static Singleton instance;
    public static String str;
    private Singleton() {}

    static Singleton getSingleInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }

    @Override
    public String toString() {
        return "{" +
                "str'" + str;
    }

    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        Singleton.str = str;
    }

}

}
