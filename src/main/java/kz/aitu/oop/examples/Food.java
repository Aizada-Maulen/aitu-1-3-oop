package kz.aitu.oop.examples.patterns.singletone;
import java.util.Scanner;
    public class Food {
        public String getType();
    }
    class Cake implements Food {

        public String getType() {
            return "Someone ordered Dessert!";
        }
    }
    class Pizza implements Food {
        public String getType() {
            return "Someone ordered Fast Food!";
        }
    }


    class FoodFactory {
        public Food getFood(String order) {
            if (order.equalsIgnoreCase("cake")) {
                Food cake = new Cake();
                return cake;
            } else {
                Food pizza = new Pizza();
                return pizza;
            }
        }
    }
    public class Factory {

        public static void main(String args[]){

            static Factory getfoodFactoryInstance(){

                Scanner sc = new Scanner(System.in);

                FoodFactory foodFactory = new FoodFactory();

                Food food = foodFactory.getFood(sc.nextLine());


                System.out.println("The factory returned " );
                System.out.println(food.getClass());
                System.out.println(food.getType());
            }
        }

    }
}
